# Deployment of metainfo Services

Follwoing figure discussed the architecture of the metainfo platfrom. 

![Alt text](metainfo-architecture.jpg?raw=true "metainfo platfrom architecture")

## Services

There are three main servies;

```
1. elassandra
2. aplos
3. gateway
```


## Deploy services

Start services in following order;

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d web
```
** After hosting website, it can be reached on `<Ip address of the Vm>:4400`.
** Open `7654` and `4400` port on VM for public

#### 1. put Employee Managment

```
# request
curl -XPOST "http://localhost:7654/api/Employee_Managment" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "111110",
  "First": "Saman",
  "Last": "Sampath",
  "Email": "saman@dms.com",
  "Phone": "0715422017",
  "Posision": "CEO",
  "Company": "DMS"
}
'

# reply
{"code":201,"msg":"Employee added"}
```


#### 2. get Employee 

```
# request
curl -XPOST "http://localhost:7654/api/Employee_Managment" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "1111xx",
  "Employee": "Saman",
  "messageType": "Addemployee",
  "userMobile": "0715422017",
}
'

# reply
{"userMobile":"0775321290","employee":"test empoyee","Company": "DMS", "timestamp":"2021-10-12 20:55:21.522"}
```
